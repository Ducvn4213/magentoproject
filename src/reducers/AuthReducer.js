import {
	MAGENTO_REGISTER_CUSTOMER,
	MAGENTO_REGISTERING_CUSTOMER,
	MAGENTO_GET_CUSTOMER_DETAIL
} from '../actions/types';

const INITIAL_STATE = {
	id: null,
	email: null,
  	firstname: null,
  	lastname: null,
  	store_id: null,
	website_is: null,
	addresses: null,
	loading: false
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case MAGENTO_REGISTER_CUSTOMER:
			return { ...state, ...action.payload };
		case MAGENTO_REGISTERING_CUSTOMER:
			return { ...state, ...action.payload };
		case MAGENTO_GET_CUSTOMER_DETAIL:
			return { ...state, ...action.payload };
		default:
			return state;
	}
};
