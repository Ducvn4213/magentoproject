import {
	MAGENTO_LOGIN_CUSTOMER,
	MAGENTO_LOGGING_IN_CUSTOMER
} from '../actions/types';

const INITIAL_STATE = {
	token: null
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case MAGENTO_LOGIN_CUSTOMER:
			return { ...state, ...action.payload };
		case MAGENTO_LOGGING_IN_CUSTOMER:
			return { ...state, ...action.payload };
		default:
			return state;
	}
};
