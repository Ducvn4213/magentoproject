import * as React from 'react';
import {
  Keyboard,
  Animated,
  View,
  Button,
  TouchableHighlight,
  Text, 
  TouchableWithoutFeedback } from 'react-native'
import { TextField } from 'react-native-material-textfield'
import { connect } from 'react-redux'
import { ProgressDialog } from 'react-native-simple-dialogs'

import PasswordTextField from './PasswordInput'

import I18n from '../../localization/I18n'
import { registerCustomer } from '../../actions'
import AppConfig from '../../config/AppConfig'

const FIRST_NAME_KEY = "FIRST_NAME_KEY"
const LAST_NAME_KEY = "LAST_NAME_KEY"
const EMAIL_KEY = "EMAIL_KEY"
const PASSWORD_KEY = "PASSWORD_KEY"
const CONFIRM_PASSWORD_KEY = "CONFIRM_PASSWORD_KEY"

class SignUp extends React.Component {
  state = {
    errorPass: null,
    errorEmailIncorrect: null,
    errorConfirmPass: null
  }

  keyboardHeight = new Animated.Value(0)
  paddingValue = 0

  //registration values
  firstName = ""
  lastName = ""
  email = ""
  password = ""
  confirmPassword = ""

  componentWillMount () {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.loading === true) { return }
    if (nextProps.auth.email === this.email) {
      setTimeout(() => {
        this.props.doAfterSignUp()
      }, 200)
      
    }
    else {
      setTimeout(() => {
        alert(nextProps.auth.message)
      }, 200)
    }
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.keyboardHeight, {
      duration: event.duration,
      toValue: - this.paddingValue,
    }).start()
  }

  keyboardWillHide = (event) => {
    Animated.timing(this.keyboardHeight, {
      duration: event.duration,
      toValue: 0,
    }).start()
  }

  _onSignUpButtonPress = () => {
    if (this.firstName.length === 0 || this.lastName.length === 0) {
      alert(I18n.t('authentication.error.name_missing'))
      return
    }

    if (this.email.length === 0) {
      alert(I18n.t('authentication.error.email_missing'))
      return
    }

    if (this.state.errorEmailIncorrect) {
      return
    }

    if (this.password.length === 0) {
      alert(I18n.t('authentication.error.pass_missing'))
      return
    }

    if (this.state.errorPass) {
      return
    }

    if (this.confirmPassword.length === 0) {
      alert(I18n.t('authentication.error.confirm_pass_missing'))
      return
    }

    if (this.state.errorConfirmPass) {
      return
    }

    const registrationInfo = {
      customer: {
        email: this.email,
        firstname: this.firstName,
        lastname: this.lastName,
      },
      password: this.password
    }
    this.props.registerCustomer(registrationInfo)
  }

  _onTextChanged = (key, text) => {
    switch (key) {
      case FIRST_NAME_KEY: { 
        this.firstName = text
        break
      }
      case LAST_NAME_KEY: {
        this.lastName = text
        break
      }
      case EMAIL_KEY: {
        this.email = text
        if (this.validateEmail(this.email) === false && this.email.length != 0) {
          this.setState({ errorEmailIncorrect: true })
        } 
        else {
          this.setState({ errorEmailIncorrect: false })
        }
        break
      }
      case PASSWORD_KEY: {
        this.password = text
        if (this.password.length <= 7 && this.password.length > 0) {
          this.setState({ errorPass: true })
        }
        else {
          this.setState({ errorPass: false })
        }
        break
      }
      case CONFIRM_PASSWORD_KEY: {
        this.confirmPassword = text
        if (this.confirmPassword !== this.password) {
          this.setState({ errorConfirmPass: true })
        }
        else {
          this.setState({ errorConfirmPass: false })
        }
        break
      }
    }
  }

  validateEmail = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    return reg.test(text)
  }

  _dismissKeyboard = () => { Keyboard.dismiss() }

  render() {
    return (
      <TouchableWithoutFeedback 
        style={styles.container} 
        onPress={this._dismissKeyboard}
      >
        <Animated.View style={[styles.container, { marginTop: this.keyboardHeight }]} >
          <TextField
            tintColor={'black'}
            label={I18n.t('authentication.fname')}
            onFocus={() => this.paddingValue = 0}
            onChangeText={text => this._onTextChanged(FIRST_NAME_KEY, text)}
          />
          <TextField
            tintColor={'black'}
            label={I18n.t('authentication.lname')}
            onFocus={() => this.paddingValue = 0}
            onChangeText={text => this._onTextChanged(LAST_NAME_KEY, text)}
          />
          <TextField
            tintColor={'black'}
            error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
            label={I18n.t('authentication.email')}
            onFocus={() => this.paddingValue = 30}
            onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
          />
          <PasswordTextField
            tintColor={'black'}
            error={this.state.errorPass ? I18n.t('authentication.error.pass_too_short') : null}
            label={I18n.t('authentication.password')}
            onFocus={() => this.paddingValue = 60}
            onChangeText={text => this._onTextChanged(PASSWORD_KEY, text)}
          />
          <PasswordTextField
            tintColor={'black'}
            error={this.state.errorConfirmPass ? I18n.t('authentication.error.confirm_pass') : null}
            label={I18n.t('authentication.confirm_pass')}
            onFocus={() => this.paddingValue = 90}
            onChangeText={text => this._onTextChanged(CONFIRM_PASSWORD_KEY, text)}
          />

          <TouchableHighlight
            onPress={this._onSignUpButtonPress}
            style={styles.button}
          >
            <Text style={styles.buttonText}>
              {I18n.t('authentication.register').toUpperCase()}
            </Text>
          </TouchableHighlight>
          <ProgressDialog 
            visible={this.props.auth.loading} 
            title={I18n.t('authentication.title').toUpperCase()}
            message={I18n.t('dialog.waiting')}
          />
        </Animated.View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  return { auth };
};

export default connect(mapStateToProps, { registerCustomer })(SignUp);

const styles = {
  container: {
    flex: 1,
    padding: 20
  },
  button: {
    marginTop: 50,
    backgroundColor: AppConfig.primaryColor,
    height: 50,
    justifyContent: 'center',
    borderRadius:25
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    margin: 10
  },
  textForget: {
    margin: 10,
    fontSize: 12,
    textAlign: 'center'
  },
  forgetButton: {
    marginTop: 20,
    justifyContent: 'center',
  }
};
