import * as React from 'react';
import {
  Keyboard,
  Animated,
  View,
  Button,
  TouchableHighlight,
  Text,
  TouchableWithoutFeedback,
  AsyncStorage
} from 'react-native';
import { TextField } from 'react-native-material-textfield'
import { connect } from 'react-redux'
import Dialog from "react-native-dialog"
import { ProgressDialog } from 'react-native-simple-dialogs'

import PasswordTextField from './PasswordInput'

import I18n from '../../localization/I18n'
import AppConfig from '../../config/AppConfig'
import { loginCustomer } from '../../actions'

const EMAIL_KEY = "EMAIL_KEY"
const PASSWORD_KEY = "PASSWORD_KEY"

class SignIn extends React.Component {
  email = ""
  password = ""

  keyboardHeight = new Animated.Value(0)
  paddingValue = 0

  state = {
    showForgetPassDialog: false,
    errorPassword: null,
    errorEmail: null
  }

  componentWillMount () {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.login.loading === true) { return }
    if (nextProps.login.token) {
      AsyncStorage.multiSet([["customer_email", this.email], ["customer_password", this.password]]).then(() => {
        nextProps.navigation.goBack()
      })
    }
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.keyboardHeight, {
      duration: event.duration,
      toValue: - this.paddingValue,
    }).start()
  }

  keyboardWillHide = (event) => {
    Animated.timing(this.keyboardHeight, {
      duration: event.duration,
      toValue: 0,
    }).start()
  }

  _onSignInButtonPress = () => {
    if (this.email.length === 0) {
      alert(I18n.t('authentication.error.email_missing'))
      return
    }

    if (this.state.errorEmail) {
      return
    }

    if (this.password.length === 0) {
      alert(I18n.t('authentication.error.pass_missing'))
      return
    }

    if (this.state.errorPassword) {
      return
    }

    this.props.loginCustomer({
      username: this.email,
      password: this.password
    })
  }

  _onTextChanged = (key, text) => {
    switch (key) {
      case EMAIL_KEY: {
        this.email = text
        if (this.validateEmail(this.email) === false && this.email.length != 0) {
          this.setState({ errorEmail: true })
        } 
        else {
          this.setState({ errorEmail: false })
        }
        break
      }
      case PASSWORD_KEY: {
        this.password = text
        if (this.password.length <= 7 && this.password.length > 0) {
          this.setState({ errorPassword: true })
        }
        else {
          this.setState({ errorPassword: false })
        }
        break
      }
    }
  }

  validateEmail = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    return reg.test(text)
  }

  _onForgetButtonPress = () => {
    this.setState({ showForgetPassDialog: true })
  }

  _doForgetPassword = () => {}

  _dismissKeyboard = () => { Keyboard.dismiss() }

  render() {
    return (
      <TouchableWithoutFeedback 
        style={styles.container} 
        onPress={this._dismissKeyboard}
      >
        <Animated.View style={[styles.container, { marginTop: this.keyboardHeight }]} >
          <TextField
            tintColor={'black'}
            error={this.state.errorEmail ? I18n.t('authentication.error.email_incorrect') : null}
            label={I18n.t('authentication.email')}
            onFocus={() => this.paddingValue = 0}
            onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
          />
          <PasswordTextField
            tintColor={'black'}
            error={this.state.errorPassword ? I18n.t('authentication.error.pass_too_short') : null}
            label={I18n.t('authentication.password')}
            onFocus={() => this.paddingValue = 20}
            onChangeText={text => this._onTextChanged(PASSWORD_KEY, text)}
          />

          <TouchableHighlight
            onPress={this._onSignInButtonPress}
            style={styles.button}
          >
            <Text style={styles.buttonText}>
              {I18n.t('authentication.sign_in').toUpperCase()}
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            underlayColor="transparent"
            onPress={this._onForgetButtonPress}
            style={styles.forgetButton}
          >
            <Text
              style={styles.textForget}
            >
              {I18n.t('authentication.forget_pass')}
            </Text>
          </TouchableHighlight>

          <Dialog.Container visible={this.state.showForgetPassDialog}>
            <Dialog.Title>{I18n.t('authentication.forget_pass_dialog_title')}</Dialog.Title>
            <Dialog.Description>
            {I18n.t('authentication.forget_pass_dialog_message')}
            </Dialog.Description>
            <Dialog.Input />
            <Dialog.Button label={I18n.t('dialog.ok')} onPress={this._doForgetPassword} />
            <Dialog.Button label={I18n.t('dialog.cancel')} onPress={() => this.setState({ showForgetPassDialog: false })} />
          </Dialog.Container>
          <ProgressDialog 
            visible={this.props.login.loading} 
            title={I18n.t('authentication.title').toUpperCase()}
            message={I18n.t('dialog.waiting')}
          />
        </Animated.View>
      </TouchableWithoutFeedback>
    );
  }
}

const mapStateToProps = ({ login }) => {
  return { login };
};

export default connect(mapStateToProps, { loginCustomer })(SignIn);

const styles = {
  container: {
    flex: 1,
    padding: 20
  },
  button: {
    marginTop: 50,
    backgroundColor: AppConfig.primaryColor,
    height: 50,
    justifyContent: 'center',
    borderRadius:25
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    margin: 10
  },
  textForget: {
    margin: 10,
    fontSize: 12,
    textAlign: 'center'
  },
  forgetButton: {
    marginTop: 10,
    marginLeft: 50,
    marginRight: 50,
    justifyContent: 'center',
  }
};
