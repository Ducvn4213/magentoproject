import * as React from 'react'
import { View, StyleSheet } from 'react-native'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view'

import SignIn from './SignIn'
import SignUp from './SignUp'

import I18n from '../../localization/I18n'

export default class Auth extends React.Component {
  static navigationOptions = {
		title: I18n.t('authentication.title').toUpperCase(),
		headerBackTitle: ' '
  }
  
  state = {
    index: 0,
    routes: [
      { key: 'sin', title: I18n.t('authentication.sign_in').toUpperCase() },
      { key: 'sup', title: I18n.t('authentication.sign_up').toUpperCase() }
    ]
  }

  _doAfterSignUp = () => {
    this.setState({ index: 0 }, () => {
      alert(I18n.t('authentication.sign_up_completed'))
    })
  }

  SignInRoute = () => (
    <SignIn {...this.props} />
  )
  SignOutRoute = () => (
    <SignUp {...this.props} doAfterSignUp={this._doAfterSignUp} />
  )

  _renderTabBar = props => {
    return <TabBar
              {...props}
              style={{ backgroundColor: 'white' }}
              labelStyle={{ color: 'black' }}
              indicatorStyle={{ backgroundColor: 'black' }} />
  }

  _renderScene = SceneMap({
    sin: this.SignInRoute,
    sup: this.SignOutRoute
  })

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={index => this.setState({ index })}
      />
    )
  }
}

const styles = {
  containerStyle: {
    flex: 1
  }
}
