import * as React from 'react'
import {
  View, Text, ScrollView
} from 'react-native'

import I18n from '../../../localization/I18n'
import AppConfig from '../../../config/AppConfig'

export default class OrderShortItem extends React.Component {
  static navigationOptions = ({ navigation }) => ({
		title: I18n.t('customer.my_order')
  })

  get data() {
    return this.props.navigation.state.params.order
  }

  get id() {
    return this.data.id
  }

  get placedDate() {
    return this.data.date
  }

  get name() {
    return this.data.name
  }

  get amount() {
    return  I18n.t('customer.total_amount') + this.data.total + '.00'
  }

  get status() {
    return this.data.status
  }

  get shippingAddress() {
    return {
      firstname: "Nguyen",
      lastname: "Duc",
      street: ["60A Hoang Van Thu"],
      city: "Ho Chi Minh",
      postcode: "700000",
      telephone: "+841678674985"
    }
  }

  get billingAddress() {
    return {
      firstname: "Nguyen",
      lastname: "Duc",
      street: ["60A Hoang Van Thu"],
      city: "Ho Chi Minh",
      postcode: "700000",
      telephone: "+841678674985"
    }
  }

  get shippingMethod() {
    return "Flat Rate - Fixed"
  }

  get paymentMethod() {
    return "Check / Money order"
  }

  get product() {
    return {
      name: "Strive Shoulder Pack",
      sku: "24-MB04",
      subtotal: 32,
      quantity: {
        ordered: 1,
        canceled: 0,
        shipping: 0,
        refunded: 0
      }
    }
  }

  // order_info: "Order",
  // shipping_address: "Shipping Address",
  // billing_address: "Billing Address",
  // shipping_method: "Shipping Method",
  // payment_method: "Payment method",
  // product_info: "Product info",
  // item_name: "Item name",
  // sky_code: "Sky code",
  // quantity: "Quantity",
  // order_detail_info: "Order detail",
  // subtotal: "subtotal",
  // shipping_handling: "Shipping & Handling",
  // total: "Grand total",
  // tax: "Tax"

  _renderShippingPart() {
    return <View style={styles.cardView}>
      <View style={styles.descriptionContainer} >
        <Text style={{ color: AppConfig.barColor, paddingLeft: 10, fontSize: 16 }}>{I18n.t('customer.order_detail.order_info').toUpperCase() + " #" + this.id}</Text>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
        <Text style={[styles.textContent, { marginTop: 10, color: 'gray' }]}>{"Placed on " + this.placedDate}</Text>
        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginRight: 10 }} >
          <Text style={styles.textStatus}>{this.status}</Text>
        </View>
      </View>
      
      <Text style={styles.textTitle}>{I18n.t('customer.order_detail.shipping_address')}</Text>
      <Text style={[styles.textContent, { marginTop: 10 }]}>{this.shippingAddress.firstname + " " + this.shippingAddress.lastname}</Text>
      <Text style={styles.textContent}>{this.shippingAddress.street[0]}</Text>
      <Text style={styles.textContent}>{this.shippingAddress.city + ", " + this.shippingAddress.postcode}</Text>
      <Text style={styles.textContent}>{'T: ' + this.shippingAddress.telephone}</Text>
      <Text style={styles.textTitle}>{I18n.t('customer.order_detail.billing_address')}</Text>
      <Text style={[styles.textContent, { marginTop: 10 }]}>{this.billingAddress.firstname + " " + this.billingAddress.lastname}</Text>
      <Text style={styles.textContent}>{this.billingAddress.street[0]}</Text>
      <Text style={styles.textContent}>{this.billingAddress.city + ", " + this.billingAddress.postcode}</Text>
      <Text style={styles.textContent}>{'T: ' + this.billingAddress.telephone}</Text>
      <Text style={styles.textTitle}>{I18n.t('customer.order_detail.shipping_method')}</Text>
      <Text style={styles.textContent}>{this.shippingMethod}</Text>
      <Text style={styles.textTitle}>{I18n.t('customer.order_detail.payment_method')}</Text>
      <Text style={styles.textContent}>{this.paymentMethod}</Text>
    </View>
  }

  _renderProductPart() {
    return <View style={styles.cardView}>
      <View style={styles.descriptionContainer} >
        <Text style={{ color: AppConfig.barColor, paddingLeft: 10, fontSize: 16 }}>{I18n.t('customer.order_detail.product_info').toUpperCase()}</Text>
      </View>
      <Text style={styles.textTitle}>{I18n.t('customer.order_detail.item_name')}</Text>
      <Text style={styles.textContent}>{this.product.name}</Text>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
        <View>
          <Text style={styles.textTitle}>{I18n.t('customer.order_detail.sky_code')}</Text>
          <Text style={styles.textContent}>{this.product.sku}</Text>
        </View>
        <View style={{marginRight: 10}} >
          <Text style={styles.textTitle}>{I18n.t('customer.order_detail.subtotal')}</Text>
          <Text style={styles.textContent}>{this.product.sku}</Text>
        </View>
      </View>
      <Text style={styles.textTitle}>{I18n.t('customer.order_detail.quantity')}</Text>
    </View>
  }

  _renderOrderDetailPart() {
    return <View style={styles.cardView}>
      <View style={styles.descriptionContainer} >
        <Text style={{ color: AppConfig.barColor, paddingLeft: 10, fontSize: 16 }}>{I18n.t('customer.order_id').toUpperCase() + this.id}</Text>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
        <Text style={[styles.textContent, { marginTop: 10, color: 'gray' }]}>{"Placed on " + this.placedDate}</Text>
        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginRight: 10 }} >
          <Text style={styles.textStatus}>{this.status}</Text>
        </View>
      </View>
      
      <Text style={styles.textTitle}>{"Shipping Address"}</Text>
      <Text style={styles.textTitle}>{"Billing Address"}</Text>
      <Text style={styles.textTitle}>{"Shipping Method"}</Text>
      <Text style={styles.textTitle}>{"Payment Method"}</Text>
      <Text style={styles.textTitle}>{"Shipping Address"}</Text>
      <Text style={styles.textContent}>{this.amount}</Text>
      
    </View>
  }

  render() {
    const item = this.data
    return <ScrollView showsVerticalScrollIndicator={false} >
      {this._renderShippingPart()}
      {this._renderProductPart()}
      {this._renderOrderDetailPart()}
    </ScrollView>
  }
}

const styles = {
  cardView: {
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 2,
    paddingBottom: 20,
    shadowColor: "#000000",
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 0.3,
    }
  },
  descriptionContainer: {
    height: 50,
    justifyContent: 'center',
    alignItems:'flex-start',
    borderTopWidth: 1,
    borderTopColor: AppConfig.backgroundColor,
    borderBottomWidth: 1,
    borderBottomColor: AppConfig.backgroundColor
  },
  textTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 15,
    paddingLeft: 10,
    color: AppConfig.barColor
  },
  textContent: {
    color: 'gray',
    marginTop: 5,
    paddingLeft: 10,
    color: AppConfig.barColor
  },
  textStatus: {
    backgroundColor: '#4286f4',
    marginTop: 10,
    marginLeft: 10,
    padding: 10,
    color: 'white'
  }
}