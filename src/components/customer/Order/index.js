import * as React from 'react'
import { FlatList } from 'react-native'
import { connect } from 'react-redux'

import OrderShortItem from './OrderShortItem'

import { Spinner } from '../../common'

import { NAVIGATION_ORDER_DETAIL_PATH } from '../../../navigators/types'

import I18n from '../../../localization/I18n'
import { getDetailCustomerInfo } from '../../../actions'
import AppConfig from '../../../config/AppConfig'

class Order extends React.Component {

  get fakeData() {
    return [{
      id: '000000423',
      date: '01/19/18',
      name: 'Peter Parker',
      total: '32',
      status: 'Pending'
    }, {
      id: '000000423',
      date: '01/19/18',
      name: 'Peter Parker',
      total: '32',
      status: 'Pending'
    }, {
      id: '000000423',
      date: '01/19/18',
      name: 'Peter Parker',
      total: '32',
      status: 'Pending'
    }]
  }

  componentDidMount() {
    this.props.getDetailCustomerInfo()  
  }

  _onOrderItemPresses = (item) => {
    this.props.navigation.navigate(NAVIGATION_ORDER_DETAIL_PATH, { order: item })
  }

  _keyExtractor = (item, index) => index

  _renderAddressItem = ({item}) => {
    return <OrderShortItem 
      data={item}
      onItemPress={this._onOrderItemPresses}
    />
  }

  render() {
    if (Array.isArray(this.props.auth.addresses)) {
      return <FlatList
        keyExtractor={this._keyExtractor}
        data={this.fakeData}
        renderItem={this._renderAddressItem}
        showsVerticalScrollIndicator={false}
      />
    }

    return (
      <Spinner />
    )
  }
}

const mapStateToProps = ({ auth }) => {
  return { auth }
};

export default connect(mapStateToProps, { getDetailCustomerInfo })(Order)