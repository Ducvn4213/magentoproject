import * as React from 'react'
import {
  View, Text, TouchableWithoutFeedback
} from 'react-native'

import I18n from '../../../localization/I18n'
import AppConfig from '../../../config/AppConfig'

export default class OrderShortItem extends React.Component {

  get id() {
    return this.props.data.id
  }

  get placedDate() {
    return this.props.data.date
  }

  get name() {
    return this.props.data.name
  }

  get amount() {
    return  I18n.t('customer.total_amount') + this.props.data.total + '.00'
  }

  get status() {
    return this.props.data.status
  }

  render() {
    const item = this.props.data
    return <TouchableWithoutFeedback onPress={() => this.props.onItemPress(this.props.data)} >
      <View style={styles.cardView}>
        <View style={styles.descriptionContainer} >
          <Text style={{ color: AppConfig.barColor, paddingLeft: 10, fontSize: 16 }}>{I18n.t('customer.order_id').toUpperCase() + this.id}</Text>
        </View>
        <Text style={[styles.textContent, { marginTop: 10, color: 'gray' }]}>{this.placedDate}</Text>
        <Text style={styles.textContent}>{this.name}</Text>
        <Text style={styles.textContent}>{this.amount}</Text>
        <View style={{ justifyContent: 'flex-start', flexDirection: 'row' }} >
          <Text style={styles.textStatus}>{this.status}</Text>
        </View>
      
      </View>
    </TouchableWithoutFeedback>
  }
}

const styles = {
  cardView: {
    height: 180,
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 2,
    shadowColor: "#000000",
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 0.3,
    }
  },
  descriptionContainer: {
    height: 50,
    justifyContent: 'center',
    alignItems:'flex-start',
    borderTopWidth: 1,
    borderTopColor: AppConfig.backgroundColor,
    borderBottomWidth: 1,
    borderBottomColor: AppConfig.backgroundColor
  },
  textContent: {
    marginTop: 5,
    paddingLeft: 10,
    color: AppConfig.barColor
  },
  textStatus: {
    backgroundColor: '#4286f4',
    marginTop: 10,
    marginLeft: 10,
    padding: 10,
    color: 'white'
  }
}