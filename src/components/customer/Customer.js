import * as React from 'react'
import { View, StyleSheet } from 'react-native'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view'

import Address from './Address'
import Order from './Order'
import Review from './Review'

import I18n from '../../localization/I18n'

export default class Customer extends React.Component {
  static navigationOptions = {
		title: I18n.t('customer.title').toUpperCase()
  }

  state = {
    index: 0,
    routes: [
      { key: 'address', title: I18n.t('customer.address').toUpperCase() },
      { key: 'order', title: I18n.t('customer.order').toUpperCase() },
      { key: 'review', title: I18n.t('customer.review').toUpperCase() }
    ]
  }

  AddressRoute = () => (
    <Address {...this.props} />
  )
  OrderRoute = () => (
    <Order {...this.props} />
  )
  ReviewRoute = () => (
    <Review {...this.props} />
  )

  _renderTabBar = props => {
    return <TabBar
              {...props}
              style={{ backgroundColor: 'white' }}
              labelStyle={{ color: 'black' }}
              indicatorStyle={{ backgroundColor: 'black' }} />
  }

  _renderScene = SceneMap({
    address: this.AddressRoute,
    order: this.OrderRoute,
    review: this.ReviewRoute
  })

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={index => this.setState({ index })}
      />
    )
  }
}

const styles = {
  containerStyle: {
    flex: 1
  }
}
