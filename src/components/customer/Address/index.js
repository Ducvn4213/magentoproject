import * as React from 'react'
import { View, FlatList } from 'react-native'
import { connect } from 'react-redux'
import { FloatingAction } from 'react-native-floating-action'
import Icon from 'react-native-vector-icons/MaterialIcons'

import AddressShortItem from './AddressShortItem'

import { Spinner } from '../../common'

import I18n from '../../../localization/I18n'
import { getDetailCustomerInfo } from '../../../actions'
import AppConfig from '../../../config/AppConfig'
import { NAVIGATION_ADD_ADDRESS_PATH } from '../../../navigators/types'

class Address extends React.Component {
  componentDidMount() {
    this.props.getDetailCustomerInfo()  
  }

  get actions() {
    return [{
      text: I18n.t('customer.add_new_address'),
      icon: <Icon name="add-location" color="white" />,
      name: 'bt_add',
      position: 1,
      color: AppConfig.primaryColor
    }]
  }

  _onPrepareToAddNew = () => {
    this.props.navigation.navigate(NAVIGATION_ADD_ADDRESS_PATH)
  }

  _onPrepareToEdit = (item) => {
    this.props.navigation.navigate(NAVIGATION_ADD_ADDRESS_PATH)
  }

  _keyExtractor = (item, index) => index

  _renderAddressItem = ({item}) => {
    return <AddressShortItem 
      data={item}
      onRequestEdit={this._onPrepareToEdit}
    />
  }

  render() {
    if (Array.isArray(this.props.auth.addresses)) {
      return <View style={{ flex: 1 }} >
        <FlatList
          keyExtractor={this._keyExtractor}
          data={this.props.auth.addresses}
          renderItem={this._renderAddressItem}
          showsVerticalScrollIndicator={false}
        />
        <FloatingAction 
          actions={this.actions} 
          color={AppConfig.primaryColor}
          onPressItem={this._onPrepareToAddNew}
          />
      </View>
    }

    return (
      <Spinner />
    )
  }
}

const mapStateToProps = ({ auth }) => {
  return { auth }
};

export default connect(mapStateToProps, { getDetailCustomerInfo })(Address)