import * as React from 'react'
import {
  View, Text, TouchableOpacity
} from 'react-native'

import I18n from '../../../localization/I18n'
import AppConfig from '../../../config/AppConfig'

export default class AddressShortItem extends React.Component {
  _getAddressType(item) {
    if (item.default_shipping === true && item.default_billing === true) {
      return I18n.t('customer.default_ship_and_bill')
    }
  
    if (item.default_shipping) {
      return I18n.t('customer.default_shipping')
    }
  
    if (item.default_billing) {
      return I18n.t('customer.default_billing')
    }
  
    return I18n.t('customer.address_custom')
  }

  render() {
    const item = this.props.data
    return <View style={styles.cardView}>
      <View style={styles.descriptionContainer} >
        <Text style={{ color: AppConfig.barColor, paddingLeft: 10, fontSize: 16 }}>{this._getAddressType(item).toUpperCase()}</Text>
      </View>
      <Text style={[styles.textContent, { marginTop: 10, fontWeight: 'bold' }]}>{item.firstname + " " + item.lastname}</Text>
      <Text style={styles.textContent}>{item.street[0]}</Text>
      <Text style={styles.textContent}>{item.city + ", " + item.postcode}</Text>
      <Text style={styles.textContent}>{'T: ' + item.telephone}</Text>
      <TouchableOpacity
        onPress={() => this.props.onRequestEdit(item)}
        style={styles.editButton}
      >
        <Text style={styles.editText} >{I18n.t('customer.edit')}</Text>
      </TouchableOpacity>
    </View>
  }
}

const styles = {
  cardView: {
    height: 150,
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 2,
    shadowColor: "#000000",
    shadowOpacity: 0.3,
    shadowRadius: 1,
    shadowOffset: {
      height: 1,
      width: 0.3,
    }
  },
  descriptionContainer: {
    height: 50,
    justifyContent: 'center',
    alignItems:'flex-start',
    borderTopWidth: 1,
    borderTopColor: AppConfig.backgroundColor,
    borderBottomWidth: 1,
    borderBottomColor: AppConfig.backgroundColor
  },
  textContent: {
    paddingLeft: 10,
    color: AppConfig.barColor
  },
  editButton: {
    position: 'absolute',
    height: 30,
    right: 20,
    bottom: 10,
    borderRadius: 15,
    borderWidth: 1,
    paddingLeft: 15, 
    paddingRight: 15,
    borderColor: AppConfig.primaryColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  editText: {
    color: AppConfig.primaryColor,
    fontSize: 12
  }
}