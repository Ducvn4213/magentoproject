import * as React from 'react';
import {
  Keyboard,
  Animated,
  ScrollView,
  View,
  Button,
  TouchableHighlight,
  Text, 
  TouchableWithoutFeedback } from 'react-native'
import { TextField } from 'react-native-material-textfield'
import { connect } from 'react-redux'
import { ProgressDialog } from 'react-native-simple-dialogs'
import CheckBox from 'react-native-check-box'

import I18n from '../../../localization/I18n'
import { registerCustomer } from '../../../actions'
import AppConfig from '../../../config/AppConfig'

const FIRST_NAME_KEY = "FIRST_NAME_KEY"
const LAST_NAME_KEY = "LAST_NAME_KEY"
const EMAIL_KEY = "EMAIL_KEY"
const PASSWORD_KEY = "PASSWORD_KEY"
const CONFIRM_PASSWORD_KEY = "CONFIRM_PASSWORD_KEY"

export default class AddNewAddress extends React.Component {
  static navigationOptions = ({ navigation }) => ({
		title: I18n.t('customer.new_address')
  })

  state = {
    errorPass: null,
    errorEmailIncorrect: null,
    errorConfirmPass: null
  }

  keyboardHeight = new Animated.Value(0)
  paddingValue = 0

  //registration values
  firstName = ""
  lastName = ""
  email = ""
  password = ""
  confirmPassword = ""

  componentWillMount () {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.loading === true) { return }
    if (nextProps.auth.email === this.email) {
      setTimeout(() => {
        this.props.doAfterSignUp()
      }, 200)
      
    }
    else {
      setTimeout(() => {
        alert(nextProps.auth.message)
      }, 200)
    }
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.keyboardHeight, {
      duration: event.duration,
      toValue: - this.paddingValue,
    }).start()
  }

  keyboardWillHide = (event) => {
    Animated.timing(this.keyboardHeight, {
      duration: event.duration,
      toValue: 0,
    }).start()
  }

  _onSaveButtonPress = () => {
    alert("add new address")
  }

  _onCheck = (key) => {
    
  }

  _onTextChanged = (key, text) => {
    switch (key) {
      case FIRST_NAME_KEY: { 
        this.firstName = text
        break
      }
      case LAST_NAME_KEY: {
        this.lastName = text
        break
      }
      case EMAIL_KEY: {
        this.email = text
        if (this.validateEmail(this.email) === false && this.email.length != 0) {
          this.setState({ errorEmailIncorrect: true })
        } 
        else {
          this.setState({ errorEmailIncorrect: false })
        }
        break
      }
      case PASSWORD_KEY: {
        this.password = text
        if (this.password.length <= 7 && this.password.length > 0) {
          this.setState({ errorPass: true })
        }
        else {
          this.setState({ errorPass: false })
        }
        break
      }
      case CONFIRM_PASSWORD_KEY: {
        this.confirmPassword = text
        if (this.confirmPassword !== this.password) {
          this.setState({ errorConfirmPass: true })
        }
        else {
          this.setState({ errorConfirmPass: false })
        }
        break
      }
    }
  }

  validateEmail = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    return reg.test(text)
  }

  _dismissKeyboard = () => { Keyboard.dismiss() }


  // fname: "First name",
  //           lname: "Last name",
  //           company: "Company",
  //           phone: "Telephone",
  //           fax: 'Fax',
  //           address: "Address",
  //           street_address_1: "Street Address 1",
  //           street_address_2: "Street Address 2",
  //           city: "City",
  //           state: "State",
  //           postcode: "Zip/PostalCode",
  //           country: "Country",
  //           use_as_default_billing: "Use as my default billing address",
  //           use_as_default_shipping: "Use as my default shipping address"

  render() {
    return (
      <TouchableWithoutFeedback 
        style={styles.container} 
        onPress={this._dismissKeyboard}
      >
        <ScrollView showsVerticalScrollIndicator={false} >
          <Animated.View style={[styles.container, { marginTop: this.keyboardHeight }]} >
            <TextField
              tintColor={'black'}
              label={I18n.t('customer.address_add.fname')}
              onFocus={() => this.paddingValue = 0}
              onChangeText={text => this._onTextChanged(FIRST_NAME_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              label={I18n.t('customer.address_add.lname')}
              onFocus={() => this.paddingValue = 0}
              onChangeText={text => this._onTextChanged(LAST_NAME_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.company')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.phone')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.fax')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.street_address_1')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.street_address_2')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.city')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.state')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.postcode')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />
            <TextField
              tintColor={'black'}
              error={this.state.errorEmailIncorrect ? I18n.t('authentication.error.email_incorrect') : null}
              label={I18n.t('customer.address_add.country')}
              onFocus={() => this.paddingValue = 30}
              onChangeText={text => this._onTextChanged(EMAIL_KEY, text)}
            />

            <CheckBox
                style={styles.checkBox}
                onClick={() => this._onCheck(EMAIL_KEY)}
                //isChecked={data.checked}
                rightText={I18n.t('customer.address_add.use_as_default_billing')}
            />
            <CheckBox
                style={styles.checkBox}
                onClick={() => this._onCheck(EMAIL_KEY)}
                //isChecked={data.checked}
                rightText={I18n.t('customer.address_add.use_as_default_shipping')}
            />

            <TouchableHighlight
              onPress={this._onSaveButtonPress}
              style={styles.button}
            >
              <Text style={styles.buttonText}>
                {I18n.t('customer.address_add.save').toUpperCase()}
              </Text>
            </TouchableHighlight>
            {/* <ProgressDialog 
              visible={this.props.auth.loading} 
              title={I18n.t('authentication.title').toUpperCase()}
              message={I18n.t('dialog.waiting')}
            /> */}
          </Animated.View>
        </ScrollView>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    padding: 20
  },
  button: {
    marginTop: 50,
    backgroundColor: AppConfig.primaryColor,
    height: 50,
    justifyContent: 'center',
    borderRadius:25
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    margin: 10
  },
  textForget: {
    margin: 10,
    fontSize: 12,
    textAlign: 'center'
  },
  forgetButton: {
    marginTop: 20,
    justifyContent: 'center',
  },
  checkBox: {
    marginTop: 20
  }
};
