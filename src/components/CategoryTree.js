import React, { Component } from 'react'
import { View, Button, Text, FlatList, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Drawer from 'react-native-drawer'

import CategoryTreeList from './CategoryTreeList'
import { Spinner } from './common'
import CategoryItem from './category_contents/CategoryItem'
import CategoryMenuItem from './category_contents/CategoryMenuItem'
import CategoryContent from './category_contents/CategoryContent'

import AppConfig from '../config/AppConfig'
import { initMagento, getCategoryTree } from '../actions'
import {
	NAVIGATION_AUTHENTICATION_PATH,
  NAVIGATION_CUSTOMER_PATH,
  NAVIGATION_CATEGORY_CONTENT_PATH
} from '../navigators/types';

const headerLeft = (onMenuTap) => <Icon.Button name="menu" backgroundColor={null} color="white" onPress={onMenuTap} />
const headerRight = (onUserTap) => <View style={{ flexDirection: 'row' }}>
  <Icon.Button name="search" backgroundColor={null} color="white"/>
  <Icon.Button name="shopping-basket" backgroundColor={null} color="white"/>
  <Icon.Button name="perm-identity" backgroundColor={null} color="white" onPress={onUserTap} />
</View>

let drawer = null
let drawerIsOpen = false

class CategoryTree extends Component {
  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state;
    return {
      headerLeft: headerLeft(() => {
        if (drawerIsOpen) {
          drawer.close()
          drawerIsOpen = false
        }
        else {
          drawer.open()
          drawerIsOpen = true
        }
      }),
		  headerRight: headerRight(() => {
        AsyncStorage.getItem("customer_email").then(email => {
          if (email) {
            navigation.navigate(NAVIGATION_CUSTOMER_PATH)
          }
          else {
            navigation.navigate(NAVIGATION_AUTHENTICATION_PATH)
          }
        })
      })
    }
  }

  componentWillMount() {
    if (!this.props.magento) {
      this.props.initMagento()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.magento && nextProps.magento) {
      this.props.getCategoryTree(nextProps.magento)
    }
  }

  _onMenuItemPress = (category) => {
    drawer.close()
    this.props.navigation.navigate(NAVIGATION_CATEGORY_CONTENT_PATH, { category })
  }

  _onCategorySelected = (category) => {
    this.props.navigation.navigate(NAVIGATION_CATEGORY_CONTENT_PATH, { category })
  }

  _renderContent() {
    const { categoryTree } = this.props;
    if (categoryTree) {
      return this._renderCategory()
    }

    return <Spinner />;
  }

  _renderCategoryItem = ({item}) => {
    return (
      <CategoryItem
        item={item}
        onItemPress={this._onCategorySelected}
      />
    )
  }

  _renderCategoryMenuItem = ({item}) => {
    return (
      <CategoryMenuItem
        item={item}
        onItemPress={this._onMenuItemPress}
      />
    )
  }

  _keyExtractor = (item, index) => index

  _renderCategory() {
    return <FlatList
      showsHorizontalScrollIndicator={false}
      horizontal={true}
      data={this.props.categoryTree.children_data}
      keyExtractor={this._keyExtractor}
      renderItem={this._renderCategoryItem}
    />
  }

  _renderMenuCategory() {
    const { categoryTree } = this.props;
    if (categoryTree) {
      return <FlatList
        showsHorizontalScrollIndicator={false}
        data={this.props.categoryTree.children_data}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderCategoryMenuItem}
      />
    }

    return <Spinner />
  }

  render() {
    return (
      <Drawer
        ref={ref => drawer = ref}
        openDrawerOffset={0.2}
        tapToClose={true}
        content={this._renderMenuCategory()}
        >
        <View style={styles.containerStyle}>
          {this._renderContent()}
        </View>
      </Drawer>
    );
  }
}

const styles = {
  containerStyle: {
    flex: 1,
    backgroundColor: '#fff'
  }
};

const mapStateToProps = ({ magento, categoryTree }) => {
  return { magento, categoryTree };
};

export default connect(mapStateToProps, { initMagento, getCategoryTree })(CategoryTree);
