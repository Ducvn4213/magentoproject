import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, FlatList, Dimensions, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import Carousel from 'react-native-snap-carousel'
import Icon from 'react-native-vector-icons/MaterialIcons'
import HtmlText from 'react-native-display-html'

import { Spinner } from '../common'
import I18n from '../../localization/I18n'

import { magento } from '../../magento'
import { cartItemProduct } from '../../actions/RestActions'
import AppConfig from '../../config/AppConfig';
import { getProductCustomAttribute } from '../../helper/product';

class ProductDetail extends Component {
  static navigationOptions = ({ navigation }) => ({
		title: navigation.state.params.product.name
  })

  state = {
    quantity: 0
  }

  componentDidMount() {
    if (this._isDownloaded() === false) {
      this.props.cartItemProduct(this.product.sku)
    }
  }

  _isDownloaded() {
    if (this.props.cart.products.sku === this.product.sku) {
      return true
    }

    return false
  }

  get product() {
    return this.props.navigation.state.params.product
  }

  get productDetail() {
    return this.props.cart.products
  }

  get price() {
    return "$" + this.product.price + ".00"
  }

  _upQuantity = () => {
    const newQuantity = this.state.quantity + 1
    this.setState({ quantity: newQuantity })
  }

  _downQuantity = () => {
    let newQuantity = this.state.quantity - 1
    if (newQuantity < 0) {
      newQuantity = 0
    }
    this.setState({ quantity: newQuantity })
  }

  _keyExtractor = (item, index) => index

  _renderImage = ({item}) => {
    const link = magento.getProductMediaUrl() + item.file
    return <Image style={styles.image} source={{ uri: link }} />
  }

  _renderImages() {
    if (this._isDownloaded() && this.productDetail.media_gallery_entries) {
      return <Carousel
        style={styles.imageContainer}
        data={this.productDetail.media_gallery_entries}
        renderItem={this._renderImage}
        sliderWidth={width}
        sliderHeight={width}
        itemWidth={width}
      />
    }
    
    return null
  }

  _renderToolbar() {
    return <View style={styles.toolbar}>
      <Icon.Button name="favorite-border" backgroundColor={null} color={AppConfig.barColor}/>
      <Icon.Button style={styles.toolbarIcon} name="share" backgroundColor={null} color={AppConfig.barColor}/>
      <Icon.Button name="email" backgroundColor={null} color={AppConfig.barColor} />
    </View>
  }

  _renderBaseInfo() {
    return <View style={{ backgroundColor: 'white', marginTop: 30 }} >
      <View style={styles.baseInfoContainer} >
        <Text style={{ color: AppConfig.barColor, marginTop: 12 }}>{this.product.name.toUpperCase()}</Text>
        <Text style={{ color: 'green', marginTop: 10 }}>{I18n.t('product.instock').toUpperCase()}</Text>
        <Text style={{ color: 'black', marginTop: 5 }}>{this.price}</Text>
      </View>
      <Text style={{ color: AppConfig.barColor, marginTop: 12, textAlign: 'center' }}>{I18n.t('product.quantity')}</Text>
      {this._renderQuantityPickup()}
    </View>
  }

  _renderQuantityPickup() {
    return <View style={styles.quantityPickupContainer} >
      {this._renderArrowButton("arrow-drop-down", this._downQuantity)}
      <View style={styles.quantityBox}>
        <Text>{this.state.quantity}</Text>
      </View>
      {this._renderArrowButton("arrow-drop-up", this._upQuantity)}
    </View>
  }

  _renderArrowButton(name, onPress) {
    return <Icon name={name} backgroundColor={null} color={AppConfig.primaryColor} size={30} onPress={onPress}/>
  }

  _renderCartAction() {
    return <View
      style={{
        height: 60,
        backgroundColor:'white',
        marginTop: 30,
        flexDirection: 'row',
        borderTopWidth: 1,
        borderTopColor: AppConfig.backgroundColor,
        borderBottomWidth: 1,
        borderBottomColor: AppConfig.backgroundColor
      }}>
      <TouchableOpacity
        style={{
          width: width / 2,
          justifyContent: 'center',
          alignItems:'center'
        }}>
        <Text style={{ color: AppConfig.primaryColor }} >{I18n.t('product.add_cart').toUpperCase()}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: width / 2,
          backgroundColor: AppConfig.primaryColor,
          justifyContent: 'center',
          alignItems:'center'
        }}>
        <Text style={{ color: 'white' }} >{I18n.t('product.buy_now').toUpperCase()}</Text>
      </TouchableOpacity>
    </View>
  }

  _renderDescription() {
    const description = getProductCustomAttribute(this.productDetail, 'description')
    return <View style={{ backgroundColor: 'white', marginTop: 30 }} >
      <View style={styles.descriptionContainer} >
        <Text style={{ color: AppConfig.barColor, paddingLeft: 10 }}>{I18n.t('product.description').toUpperCase()}</Text>
      </View>
      <HtmlText htmlString={description.value} />
    </View>
  }

  _renderContent() {
    if (this._isDownloaded()) {
      return <View style={styles.container}>
        {this._renderImages()}
        {this._renderToolbar()}
        {this._renderBaseInfo()}
        {this._renderCartAction()}
        {this._renderDescription()}
      </View>
    }

    return <Spinner style={{ marginTop: 50 }} />
  }

  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false} >
        {this._renderContent()}
      </ScrollView>
    )
  }
}

const mapStateToProps = ({ cart }) => {
  return { cart: cart };
};

export default connect(mapStateToProps, { cartItemProduct })(ProductDetail);

const { width } = Dimensions.get('window')
const styles = {
  container: {
    //flex: 1
  },
  imageContainer: {
    height: width
  },
  image: {
    width,
    height: width
  },
  toolbar: {
    height: 50,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    borderTopWidth: 1,
    borderTopColor: AppConfig.backgroundColor,
  },
  toolbarIcon: {
    marginLeft: 50,
    marginRight: 50
  },
  quantityBox: {
    marginLeft: 20, 
    marginRight: 20,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems:'center',
    borderWidth: 1,
    borderColor: AppConfig.backgroundColor
  },
  descriptionContainer: {
    height: 50,
    justifyContent: 'center',
    alignItems:'flex-start',
    borderTopWidth: 1,
    borderTopColor: AppConfig.backgroundColor,
    borderBottomWidth: 1,
    borderBottomColor: AppConfig.backgroundColor
  },
  baseInfoContainer: {
    height: 90,
    alignItems:'center',
    borderTopWidth: 1,
    borderTopColor: AppConfig.backgroundColor,
    borderBottomWidth: 1,
    borderBottomColor: AppConfig.backgroundColor
  },
  quantityPickupContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
  }
}