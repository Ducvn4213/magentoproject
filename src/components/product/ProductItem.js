import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback, Dimensions } from 'react-native'
// import Rating from 'react-native-star-rating'

import I18n from '../../localization/I18n'

import AppConfig from '../../config/AppConfig';
import { getProductThumbnailFromAttribute } from '../../helper/product';

export default class ProductItem extends Component {
  render() {
    return (
      <TouchableWithoutFeedback onPress={() => this.props.onItemPress(this.props.product)}>
        <View style={styles.container}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={{ uri: this.image }}
            />
          </View>
          <View style={styles.informationContainer}>
            <Text style={[styles.text, { marginTop: 5 }]}>{this.name}</Text>
            {/* <Rating
              containerStyle={styles.rating}
              disabled={true}
              maxStars={5}
              rating={2}
              fullStarColor='yellow'
              halfStarColor='yellow'
              emptyStarColor='gray'
              starSize={18}
            /> */}
            <Text style={[styles.text, styles.textPrice]}>{this.price}</Text>
          </View>
          <TouchableOpacity
            style={styles.addToCartButton}
          >
            <Text style={styles.addToCartText} >{I18n.t('product.add_to_cart')}</Text>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  get image() {
    return getProductThumbnailFromAttribute(this.props.product)
  }

  get name() {
    return this.props.product.name
  }

  get price() {
    return "$" + this.props.product.price + ".00"
  }
}

const { width } = Dimensions.get('window')
const styles = {
  container: {
    width: width / 2,
    height: width * 2 / 3,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: AppConfig.backgroundColor,
    borderLeftWidth: 1,
    borderLeftColor: AppConfig.backgroundColor
  },
  imageContainer: {
    height: width * 2 / 5
  },
  image: {
    height: width * 2 / 5
  },
  informationContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  rating: {
    width: 100,
    marginTop: 10,
    marginBottom: 5
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    color: AppConfig.barColor
  },
  textPrice: {
    marginTop: 5,
    fontSize: 12
  },
  addToCartButton: {
    position: 'absolute',
    width: width / 2 - 40,
    height: 30,
    left: 20,
    bottom: 5,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: AppConfig.primaryColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  addToCartText: {
    color: AppConfig.primaryColor,
    fontSize: 12
  }
}