import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';

import AppConfig from '../../config/AppConfig'

export default class CategoryItem extends Component {
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.onItemPress(this.props.item)}
      >
        <View 
        style={styles.container}
        >
          <View
            style={styles.titleContainer}
          >
            <Text
              style={styles.title}
            >
              {this.props.item.name}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  container: {
    width: 150,
    height: 150,
    margin: 3,
    backgroundColor: AppConfig.barColor
  },
  titleContainer: {
    backgroundColor: AppConfig.primaryColor + '80',
    width: 150,
    height: 40,
    marginTop: 110,
    justifyContent: 'center'
  },
  title: {
    textAlign: 'center',
    color: 'white'
  }
}
