import React, { Component } from 'react';
import { View, Text, Image, TouchableWithoutFeedback } from 'react-native';

import AppConfig from '../../config/AppConfig'

export default class CategoryItem extends Component {
  render() {
    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.onItemPress(this.props.item)}
      >
        <View 
        style={styles.container}
        >
          <Image
            style={styles.image}
          />
          <View 
            style={styles.titleContainer}
          >
            <Text
              style={styles.title}
            >
              {this.props.item.name}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  container: {
    height: 50,
    margin: 3,
    flexDirection: 'row'
  },
  image: {
    width: 40,
    height: 40,
    backgroundColor: AppConfig.barColor
  },
  titleContainer: {
    height: 40,
    marginLeft: 5,
    justifyContent: 'center'
  },
  title: {
    color: AppConfig.barColor
  }
}
