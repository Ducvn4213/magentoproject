import React, { Component } from 'react'
import { View, Text, ScrollView, FlatList, Dimensions } from 'react-native'
import { connect } from 'react-redux'

import CategoryItem from './CategoryItem'

import GridView from '../common/GridView'
import ProductItem from '../product/ProductItem'

import { getProductsForCategory } from '../../actions'
import {
  NAVIGATION_CATEGORY_CONTENT_PATH,
  NAVIGATION_PRODUCT_DETAIL_PATH
} from '../../navigators/types';
import AppConfig from '../../config/AppConfig'
import { Spinner } from '../common';

class CategoryContent extends Component {
  static navigationOptions = ({ navigation }) => ({
		title: navigation.state.params.category.name
  })

  state = {
    loading: true
  }
  
  componentDidMount() {
    this.props.getProductsForCategory({id: this.category.id})
  }

  componentWillUnmount() {
    if (this.needUpdateProducts) {
      this.needUpdateProducts()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.loading === false) {
      this.setState({ loading: false })
    }
  }

  _needUpdateProducts = () => {
    this.setState({ loading: true }, () => { this.props.getProductsForCategory({id: this.category.id}) })
  }

  get category() {
    return this.props.navigation.state.params.category
  }

  get needUpdateProducts() {
    return this.props.navigation.state.params.needUpdateProducts
  }

  _onCategorySelected = (category) => {
    const needUpdateProducts = this._needUpdateProducts
    this.props.navigation.navigate(NAVIGATION_CATEGORY_CONTENT_PATH, { category, needUpdateProducts })
  }

  _onProductPress = (product) => {
    this.props.navigation.navigate(NAVIGATION_PRODUCT_DETAIL_PATH, { product })
  }

  _keyExtractor = (item, index) => index

  _renderBanner() {
    return <View style={styles.bannerContainer}>
              <Text style={styles.bannerText}>BANNER</Text>
            </View>
  }

  _renderCategoryItem = ({item}) => {
    return (
      <CategoryItem
        item={item}
        onItemPress={this._onCategorySelected}
      />
    )
  }

  _renderCategories() {
    if (this.category.children_data.length != 0) {
      return <FlatList
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        data={this.category.children_data}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderCategoryItem}
      />
    }

    return null
  }

  _renderProducts() {
    if (this.state.loading === false) {
      return <GridView
        data={this.props.products}
        itemsPerRow={2}
        renderItem={this._renderProductItem}
      />
    }
    
    return <Spinner style={{ marginTop: 100 }} />
  }

  _renderProductItem = (item, index) => {
    return <ProductItem 
      product={item}
      onItemPress={this._onProductPress}
    />
  }

  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.container}
      >
        <View>
          {this._renderBanner()}
          {this._renderCategories()}
          {this._renderProducts()}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = ({ category }) => {
  return { products: category.products, loading: category.loading };
};

export default connect(mapStateToProps, { getProductsForCategory })(CategoryContent);

const { width } = Dimensions.get('window')
const styles = {
  container: {
    flex: 1,
    backgroundColor: AppConfig.backgroundColor
  },
  bannerContainer: {
    justifyContent: 'center',
    height: 200,
    backgroundColor: 'white'
  },
  bannerText: {
    textAlign: 'center',
    fontSize: 30,
    color: AppConfig.barColor
  },
  productItem: {
    width: width / 2,
    height: width * 2 / 3,
    backgroundColor: 'red'
  }
}