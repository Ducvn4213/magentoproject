import React from 'react';
import { FlatList, View, StyleSheet } from 'react-native';

export default class GridView extends React.Component {

  groupItems = (items, itemsPerRow) => {
    var itemsGroups = []
    var group = []
    items.forEach((item, index) => {
      if (group.length === itemsPerRow) {
        itemsGroups.push(group)
        group = [item]
      } else {
        group.push(item)
      }
    })

    if (group.length > 0) {
      itemsGroups.push(group)
    }

    return itemsGroups;
  }

  keyExtractor = (item, index) => index

  renderGroup = (group) => {
    var items = group.item.map((_item, index) => {
      return this.renderItem(_item, index)
    })

    return (
      <View style={styles.group}>
        {items}
      </View>
    )
  }

  renderItem = (item, index) => {
    return this.props.renderItem(item, index)
  }

  render() {
    const groups = this.groupItems(this.props.data, this.props.itemsPerRow);
    return (
      <FlatList
        {...this.props}
        data={groups}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderGroup}
      />
    )
  }
}

const styles = StyleSheet.create({
  group: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden'
  }
})