export default appConfig = {
  barColor: '#443030',
  primaryColor: '#ff3f92',
  secondaryColor: '#fff',
  backgroundColor: '#e3e3e3'
};
