import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';
import ProductList from '../components/catalog/ProductList';
import CategoryTree from '../components/CategoryTree';
import Product from '../components/catalog/Product';
import Cart from '../components/cart/Cart';
import Checkout from '../components/checkout/Checkout';
import Auth from '../components/auth/Auth'
import Customer from '../components/customer/Customer'
import CategoryContent from '../components/category_contents/CategoryContent'
import AppConfig from '../config/AppConfig'
import ProductDetail from '../components/product/ProductDetail'
import OrderDetail from '../components/customer/Order/OrderDetail'
import AddNewAddress from '../components/customer/Address/AddNewAddress'

export const AppNavigator = StackNavigator({
    AddNewAddress: { screen: AddNewAddress },
    OrderDetail: { screen: OrderDetail },
    ProductDetail: { screen: ProductDetail },
    CategoryContent: { screen: CategoryContent },
    Auth: { screen: Auth },
    Customer: { screen: Customer },
		CategoryTree: { screen: CategoryTree },
		Category: { screen: ProductList },
		Product: { screen: Product },
		Cart: { screen: Cart },
    Checkout: { screen: Checkout },
	},
  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: AppConfig.barColor,
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: '500',
				fontSize: 18,
        alignSelf: 'center'
      },
    },
  }
);

const AppWithNavigationState = ({ dispatch, nav }) => (
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
